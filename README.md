# J2EEScan - J2EE Security Scanner Burp Suite Plugin

## What is J2EEScan
J2EEScan is a plugin for [Burp Suite Proxy](http://portswigger.net/). 
The goal of this plugin is to improve the test coverage during 
web application penetration tests on J2EE applications. 


## This repository has been moved to github https://github.com/ilmila/J2EEScan
